FROM node:alpine
ADD . /code
WORKDIR /code
COPY . .
RUN npm install
CMD npm run dev
